from PIL import Image
import os
images=os.listdir('.')
images = [file for file in images if os.path.splitext(file)[1].lower() != '.py']
for image in images:
    img = Image.open(image)
    img.thumbnail((400, 400))
    img.save(f'{image[:-4]}_mini.png')
